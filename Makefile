DEVICE = hx1k

all: toplevel.bin

toplevel.bin: master.rpt master.bin worker.rpt worker.bin
	tools/icestorm/icemulti/icemulti -o toplevel.bin -v -p0 worker.bin master.bin

master.blif: master.v
	tools/yosys/yosys -p 'synth_ice40 -top master -blif $@' $<

worker.blif: worker.v
	tools/yosys/yosys -p 'synth_ice40 -top worker -blif $@' $<

%.asc: %.pcf %.blif
	tools/arachne-pnr/bin/arachne-pnr -d $(subst hx,,$(subst lp,,$(DEVICE))) -o $@ -p $^ -P tq144 -s 4

%.bin: %.asc
	tools/icestorm/icepack/icepack $< $@

%.rpt: %.asc
	tools/icestorm/icetime/icetime -C tools/icestorm/icebox/chipdb-$(subst hx,,$(subst lp,,$(DEVICE))).txt -d $(DEVICE) -mtr $@ $<

prog: toplevel.bin
	tools/icestorm/iceprog/iceprog $<

progall: toplevel.bin
	bash progall.sh

progmaster: master.bin
	tools/icestorm/iceprog/iceprog $<

clean:
	rm -f master.blif master.asc worker.blif worker.asc master.bin worker.bin toplevel.bin


sim:
	tools/yosys/yosys -p 'read_verilog -sv -DSIM worker.v; prep -top worker -nordff; sim -clock CLKin -vcd test.vcd -n 3000'

.PHONY: all prog clean sim
