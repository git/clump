`include "master.v"
`include "worker.v"

module toplevel (input CLKin, output [4:0] led);
   wire worker_tx;
   wire worker_rx;

   wire worker_ready;
   wire master_ready;

   wire [4:0] worker_led;
   wire [4:0] master_led;

   worker worker (.CLKin(CLKin), .led(worker_led), .uart_tx(worker_tx), .uart_rx(worker_rx), .ready_out(worker_ready), .ready_in(master_ready));

   master master (.CLKin(CLKin), .led(master_led), .uart_tx(worker_rx), .uart_rx(worker_tx), .ready_out(master_ready), .ready_in(worker_ready));

   assign led = worker_led | master_led;
endmodule
