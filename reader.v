`define STATE_IDLE     3'd0
`define STATE_LENGTH   3'd1
`define STATE_READ1    3'd2
`define STATE_READ2    3'd3
`define STATE_WRITE    3'd4
`define STATE_FINISHED 3'd5

module READER (input clk, input clk_enable, input [7:0] rx_byte, input rx_signal, output finished, output ram_we, output [12:0] ram_addr, output reg [15:0] ram_di);
   reg [2:0] state = `STATE_IDLE;

   reg [12:0] total_words;
   reg [12:0] current_index;

   assign ram_addr = current_index;
   assign finished = state == `STATE_FINISHED;
   assign ram_we   = state == `STATE_WRITE;

   always @ (posedge clk)
	 if (clk_enable) begin
		case(state)
		  `STATE_IDLE: begin
			 if(rx_signal) begin
				total_words[12:8] <= rx_byte[4:0];
				current_index <= -1;
				state <= `STATE_LENGTH;
			 end
		  end

		  `STATE_LENGTH: begin
			 if(rx_signal) begin
				total_words[7:0] <= rx_byte;
				state <= `STATE_READ1;
			 end
		  end

		  `STATE_READ1: begin
			 if(rx_signal) begin
				ram_di[15:8] <= rx_byte;
				current_index <= current_index + 1;
				state <= `STATE_READ2;
			 end
		  end

		  `STATE_READ2: begin
			 if(rx_signal) begin
				ram_di[7:0] <= rx_byte;
				state <= `STATE_WRITE;
			 end
		  end

		  `STATE_WRITE: begin
			 if(current_index + 1 == total_words) begin
				state <= `STATE_FINISHED;
			 end else begin
				state <= `STATE_READ1;
			 end
		  end

		  `STATE_FINISHED: state <= `STATE_IDLE;
		  default:         state <= `STATE_IDLE;
		endcase
	 end // if (clk_enable)
endmodule
