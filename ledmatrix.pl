#!/usr/bin/perl
use v5.14;
use warnings;
use lib '.';

use asm;

storei 0, 0xF;
route 0, 9, 1;
ledi 1;
news_mm 0, 1, -1, 0;
route 1, 9, 1;
ledi 2;
news_mm 1, 2, -1, 0;
route 2, 9, 1;
ledi 4;
news_mm 2, 3, -1, 0;
route 3, 9, 1;
ledi 8;
