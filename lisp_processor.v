`include "pll.v"
`include "gc.v"
`include "eval.v"
`include "reader.v"
`include "uart.v"
`include "writer.v"
`include "controller.v"

`define GCOP_NOP      4'd0
`define GCOP_CDR      4'd1
`define GCOP_CAR      4'd2
`define GCOP_CDRQ     4'd3
`define GCOP_CARQ     4'd4
`define GCOP_CARR     4'd5
`define GCOP_CDRRX    4'd6
`define GCOP_CARRX    4'd7
`define GCOP_CDRQX    4'd8
`define GCOP_CONS     4'd9
`define GCOP_XCONS    4'd10
`define GCOP_RPLACDR  4'd11
`define GCOP_LDQ      4'd12
`define GCOP_RDQ      4'd13
`define GCOP_RDQA     4'd14
`define GCOP_RDQCDRRX 4'd15

`ifdef SIM
 `define UART_DIVIDE 1
`else
 `define UART_DIVIDE 3
`endif

module cpu (input CLKin, output [4:0] led, output uart_tx, input uart_rx);
   wire clk;

   pll pll (.clock_in(CLKin), .clock_out(clk));

   wire [12:0] freeptr;
   wire [15:0] E1;
   wire [15:0] E2;
   wire [3:0] gcop;
   wire [5:0] gostate;
   wire [5:0] eostate;
   wire 	  conn_ea;
   wire 	  conn_et;

   wire 	  step_eval;

   wire        gc_ram_we;
   wire [12:0] gc_ram_addr;
   wire [15:0] gc_ram_di;

   wire        reader_ram_we;
   wire [12:0] reader_ram_addr;
   wire [15:0] reader_ram_di;

   wire [12:0] writer_ram_addr;

   wire        ram_we;
   wire [12:0] ram_addr;
   wire [15:0] ram_di;
   wire [15:0] ram_do;

   wire 	   eval_finished;
   wire 	   reader_finished;
   wire		   writer_finished;

   wire 	   gc_clock_enable;
   wire 	   eval_clock_enable;
   wire 	   reader_clock_enable;
   wire 	   writer_clock_enable;
   wire 	   reset;

   CTRL ctrl (.clk(clk), .step_eval(step_eval), .reader_finished(reader_finished), .eval_finished(eval_finished), .writer_finished(writer_finished), .gc_clock_enable(gc_clock_enable), .eval_clock_enable(eval_clock_enable), .reader_clock_enable(reader_clock_enable), .writer_clock_enable(writer_clock_enable), .reset(reset), .gc_ram_we(gc_ram_we), .reader_ram_we(reader_ram_we), .gc_ram_addr(gc_ram_addr), .reader_ram_addr(reader_ram_addr), .writer_ram_addr(writer_ram_addr), .gc_ram_di(gc_ram_di), .reader_ram_di(reader_ram_di), .ram_we(ram_we), .ram_addr(ram_addr), .ram_di(ram_di), .uart_is_receiving(uart_is_receiving), .uart_is_transmitting(uart_is_transmitting), .led(led));

   GCRAM gcram (.clk(clk), .we(ram_we), .addr(ram_addr), .di(ram_di), .do(ram_do));

   GC gc (.clk(clk), .clk_enable(gc_clock_enable), .Ein(E1), .Eout(E2), .gcop(gcop), .ostate(gostate), .step_eval(step_eval), .conn_ea(conn_ea), .conn_et(conn_et), .ram_we(gc_ram_we), .ram_addr(gc_ram_addr), .ram_di(gc_ram_di), .ram_do(ram_do), .freeptr(freeptr));

   EVAL eval (.clk(clk), .rst(reset), .clk_enable(eval_clock_enable), .Ein(E2), .Eout(E1), .gcop(gcop), .ostate(eostate), .conn_ea(conn_ea), .conn_et(conn_et), .eval_finished(eval_finished));

   READER reader (.clk(clk), .clk_enable(reader_clock_enable), .rx_byte(uart_rx_byte), .rx_signal(uart_rx_signal), .finished(reader_finished), .ram_we(reader_ram_we), .ram_addr(reader_ram_addr), .ram_di(reader_ram_di));

   WRITER writer (.clk(clk), .clk_enable(writer_clock_enable), .tx_byte(uart_tx_byte), .tx_signal(uart_tx_signal), .tx_busy(uart_is_transmitting), .finished(writer_finished), .ram_addr(writer_ram_addr), .ram_do(ram_do), .freeptr(freeptr));

   // UART outputs
   wire       uart_rx_signal;
   wire [7:0] uart_rx_byte;
   wire       uart_is_receiving;
   wire       uart_is_transmitting;
   wire       uart_rx_error;

   // UART logic
   wire 	  uart_tx_signal;
   wire [7:0] uart_tx_byte;

   // 19200 baud uart
   uart #(.CLOCK_DIVIDE(`UART_DIVIDE)) uart (.clk(clk), .rx(uart_rx), .tx(uart_tx), .transmit(uart_tx_signal), .tx_byte(uart_tx_byte), .received(uart_rx_signal), .rx_byte(uart_rx_byte), .is_receiving(uart_is_receiving), .is_transmitting(uart_is_transmitting), .recv_error (uart_rx_error));
endmodule
