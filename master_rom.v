// ROM module with single input addr, output port, and clock input.
// Data is clocked out of the ROM on positive clock edges.

module master_rom (input clk, input [7:0] addr, output reg [31:0] data);
   reg [31:0] rom [0:255];
   initial $readmemh("code.hex", rom);

   always @ (posedge clk) begin
	  data <= rom[addr];
   end
endmodule
