module EVAL(input clk, input clk_enable, input rst, input [15:0] Ein, output [15:0] Eout, output [3:0] gcop, output [5:0] ostate, output eval_finished, input conn_ea, input conn_et);
   reg [22:0] rom_output;
   reg [5:0]  eostate;
   reg [5:0]  enstate;

`ifdef SIM
   initial eostate <= 0;
`endif

   assign eval_finished = rom_output[22];
   wire et_lit = rom_output[21];
   wire ea_lit = rom_output[20];
   wire eaz_etz_eqv_disp = rom_output[19];
   wire et_disp = rom_output[18];
   wire rdC = rom_output[17];
   wire rdL = rom_output[16];
   wire rdN = rom_output[15];
   wire rdXp = rom_output[14];
   wire rdX = rom_output[13];
   wire rdV = rom_output[12];
   wire ldC = rom_output[11];
   wire ldL = rom_output[10];
   wire ldN = rom_output[9];
   wire ldX = rom_output[8];
   wire ldV = rom_output[7];
   assign gcop = rom_output[6:3];
   wire [2:0] lit = rom_output[2:0];

   wire 	  et_zero = ~|E[15:13];
   wire 	  ea_zero = ~|E[12:0];

   always @* begin
	  case(eostate)
		6'o00:   begin rom_output <= 23'o14004002; enstate <= 6'o01; end
		6'o01:   begin rom_output <= 23'o14001000; enstate <= 6'o33; end
		6'o02:   begin rom_output <= 23'o02040200; enstate <= 6'o23; end
		6'o03:   begin rom_output <= 23'o00400120; enstate <= 6'o20; end
		6'o04:   begin rom_output <= 23'o00010110; enstate <= 6'o64; end
		6'o05:   begin rom_output <= 23'o00001170; enstate <= 6'o22; end
		6'o06:   begin rom_output <= 23'o14000013; enstate <= 6'o71; end
		6'o07:   begin rom_output <= 23'o00000000 | (1 << 22); enstate <= 6'o07; end
		6'o10:   begin rom_output <= 23'o00020200; enstate <= 6'o45; end
		6'o11:   begin rom_output <= 23'o00020200; enstate <= 6'o45; end
		6'o12:   begin rom_output <= 23'o00100140; enstate <= 6'o02; end
		6'o13:   begin rom_output <= 23'o00020200; enstate <= 6'o45; end
		6'o14:   begin rom_output <= 23'o00020010; enstate <= 6'o53; end
		6'o15:   begin rom_output <= 23'o00020010; enstate <= 6'o03; end
		6'o16:   begin rom_output <= 23'o14002000; enstate <= 6'o40; end
		6'o17:   begin rom_output <= 23'o00020010; enstate <= 6'o30; end
		6'o20:   begin rom_output <= 23'o00100110; enstate <= 6'o21; end
		6'o21:   begin rom_output <= 23'o10004161; enstate <= 6'o61; end
		6'o22:   begin rom_output <= 23'o00004030; enstate <= 6'o24; end
		6'o23:   begin rom_output <= 23'o00010500; enstate <= 6'o02; end
		6'o24:   begin rom_output <= 23'o02010070; enstate <= 6'o33; end
		6'o25:   begin rom_output <= 23'o00000040; enstate <= 6'o26; end
		6'o26:   begin rom_output <= 23'o00010120; enstate <= 6'o30; end
		6'o27:   begin rom_output <= 23'o00000240; enstate <= 6'o45; end
		6'o30:   begin rom_output <= 23'o00000350; enstate <= 6'o45; end
		6'o31:   begin rom_output <= 23'o00001150; enstate <= 6'o32; end
		6'o32:   begin rom_output <= 23'o02200010; enstate <= 6'o63; end
		6'o33:   begin rom_output <= 23'o01000440; enstate <= 6'o10; end
		6'o34:   begin rom_output <= 23'o00201000; enstate <= 6'o35; end
		6'o35:   begin rom_output <= 23'o00010020; enstate <= 6'o62; end
		6'o36:   begin rom_output <= 23'o00400120; enstate <= 6'o54; end
		6'o37:   begin rom_output <= 23'o01000430; enstate <= 6'o10; end
		6'o40:   begin rom_output <= 23'o00020010; enstate <= 6'o36; end
		6'o41:   begin rom_output <= 23'o00010020; enstate <= 6'o30; end
		6'o42:   begin rom_output <= 23'o00010010; enstate <= 6'o30; end
		6'o43:   begin rom_output <= 23'o00200010; enstate <= 6'o25; end
		6'o44:   begin rom_output <= 23'o02010000; enstate <= 6'o55; end
		6'o45:   begin rom_output <= 23'o01400020; enstate <= 6'o04; end
		6'o46:   begin rom_output <= 23'o00200200; enstate <= 6'o45; end
		6'o47:   begin rom_output <= 23'o00010010; enstate <= 6'o31; end
		6'o50:   begin rom_output <= 23'o00000060; enstate <= 6'o51; end
		6'o51:   begin rom_output <= 23'o00004030; enstate <= 6'o52; end
		6'o52:   begin rom_output <= 23'o01000450; enstate <= 6'o40; end
		6'o53:   begin rom_output <= 23'o00100120; enstate <= 6'o70; end
		6'o54:   begin rom_output <= 23'o00100110; enstate <= 6'o56; end
		6'o55:   begin rom_output <= 23'o14000201; enstate <= 6'o45; end
		6'o56:   begin rom_output <= 23'o00200110; enstate <= 6'o60; end
		6'o57:   begin rom_output <= 23'o14000200; enstate <= 6'o45; end
		6'o60:   begin rom_output <= 23'o10004160; enstate <= 6'o61; end
		6'o61:   begin rom_output <= 23'o00020020; enstate <= 6'o62; end
		6'o62:   begin rom_output <= 23'o01000550; enstate <= 6'o10; end
		6'o63:   begin rom_output <= 23'o02000030; enstate <= 6'o63; end
		6'o64:   begin rom_output <= 23'o00002150; enstate <= 6'o65; end
		6'o65:   begin rom_output <= 23'o00400010; enstate <= 6'o66; end
		6'o66:   begin rom_output <= 23'o00001040; enstate <= 6'o50; end
		6'o67:   begin rom_output <= 23'o00100130; enstate <= 6'o34; end
		6'o70:   begin rom_output <= 23'o10000363; enstate <= 6'o45; end
		6'o71:   begin rom_output <= 23'o00010130; enstate <= 6'o07; end
		default: begin rom_output <= 23'o00000000; enstate <= 6'o07; end
	  endcase
   end // always @ *

   always @ (posedge clk) begin
	  if (rst)
		eostate = 0;
	  if (clk_enable) begin
		 eostate <=
				   et_disp ? (enstate | E[15:13]) :
				   eaz_etz_eqv_disp ? (enstate | {ea_zero, et_zero, 1'b0}) :
				   enstate;
	  end
   end

   assign ostate = eostate;

   reg [15:0] V;
   reg [15:0] X;
   reg [15:0] N;
   reg [15:0] L;
   reg [15:0] C;

   wire [15:0] E;

   wire [15:0] EfromV = rdV ? V : 0;
   wire [15:0] EfromX = rdX ? X : 0;
   wire [15:0] EfromXp = rdXp ? {X[15:13], X[12:0] + 1} : 0;
   wire [15:0] EfromN = rdN ? N : 0;
   wire [15:0] EfromL = rdL ? L : 0;
   wire [15:0] EfromC = rdC ? C : 0;
   wire [12:0] EAfromEin = conn_ea ? Ein[12:0] : 0;
   wire [2:0]  ETfromEin = conn_et ? Ein[15:13] : 0;
   wire [15:0] EfromEin  = {ETfromEin, EAfromEin};
   wire [12:0] EAfromLIT = ea_lit ? {8'b0, lit, 1'b0} : 0;
   wire [2:0]  ETfromLIT = et_lit ? lit : 0;
   wire [15:0] EfromLIT = {ETfromLIT, EAfromLIT};

   assign E = EfromV | EfromX | EfromXp | EfromN | EfromL | EfromC | EfromLIT | EfromEin;
   assign Eout = EfromV | EfromX | EfromXp | EfromN | EfromL | EfromC | EfromLIT;

   always @ (posedge clk) begin
	  if (clk_enable) begin
		 if (ldV) V <= E;
		 if (ldX) X <= E;
		 if (ldN) N <= E;
		 if (ldL) L <= E;
		 if (ldC) C <= E;
	  end
   end
endmodule // EVAL
