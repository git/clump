`include "news.v"

`define OP_NOP    3'd0
`define OP_LOADA  3'd1
`define OP_LOADB  3'd2
`define OP_STORE  3'd3
`define OP_STOREI 3'd4
`define OP_LOADI  3'd5
`define OP_ROUTE  3'd6
`define OP_LED    3'd7

`define DIRECTION_N  3'd0
`define DIRECTION_NE 3'd1
`define DIRECTION_E  3'd2
`define DIRECTION_SE 3'd3
`define DIRECTION_S  3'd4
`define DIRECTION_SW 3'd5
`define DIRECTION_W  3'd6
`define DIRECTION_NW 3'd7

module chip(input clk, input [2:0] op, input [15:0] I, output reg [15:0] mem_in, input [15:0] mem_out, output reg mem_write, output reg [3:0] led_out = 0);

   // parity is unimplemented

   // OP_LOADA
   wire [3:0] flagr = I[3:0];
   wire 	  bsel = I[4];
   wire [0:7] aluc = I[12:5];

   // OP_LOADB
   wire [3:0] cond = I[3:0];
   wire 	  inv = I[4];
   wire [0:7] alus = I[12:5];

   // OP_STORE
   wire [3:0] flagw = I[3:0];
   wire 	  edge_ = I[7];
   wire [3:0] cube = I[11:8];

   // OP_LED
   wire 	  mode   = I[4];
   wire [1:0] offset = I[1:0];
   wire [3:0] leds   = I[3:0];


   reg [15:0] A = 0;
   reg [15:0] B = 0;
   reg [15:0] C = 0;
   reg [15:0] F = 0;
   reg [15:0] Cond = 0;
   reg [15:0] R = 0;
   reg [7:0]  alu_sum = 0;
   reg [7:0]  alu_carry = 0;
   reg [15:0] cube_in;

   // these are not really regs

   reg [15:0]  alu_sum_out;
   reg [15:0]  alu_carry_out;

   reg [2:0]   alu_index [15:0];

   reg [15:0]  idx;

   always @* begin
	  for(idx = 0; idx < 16; idx=idx+1) begin
		 alu_index[idx] = (A[idx] << 2) + (B[idx] << 1) + F[idx];
		 alu_sum_out[idx] <= alu_sum[alu_index[idx]];
		 alu_carry_out[idx] <= alu_carry[alu_index[idx]];
	  end
   end

   reg [3:0] flags_addr_latch;
   reg [3:0] flags_addr;

   always @* begin
	  if(flags_addr_latch)
		flags_addr <= flags_addr_latch;
	  else
		case(op)
		  `OP_LOADA:
			flags_addr <= flagr;
		  `OP_LOADB:
			flags_addr <= cond;
		  `OP_STORE:
			flags_addr <= flagw;
		  default:
			flags_addr <= 0;
		endcase
   end // always @ *

   reg  [15:0] flags_in;
   wire [15:0] flags_out;
   reg 		   flags_write;

   reg [15:0]  latest_news;

   RAM #(.ADDRESS_BITS(3)) flags (.clk(clk), .write(flags_write), .addr(flags_addr[2:0]), .in(flags_in), .out(flags_out));

   reg [15:0]  flag_or_news;
   reg [15:0]  news_out;

   news newspaper (.news_in(latest_news), .direction(flags_addr[2:0]), .news_out(news_out));

   assign flag_or_news = flags_addr[3] ? news_out : flags_out;

   always @ (posedge clk) begin
	  if(mem_write)
		mem_write <= 0;
	  if(flags_write) begin
		 flags_write <= 0;
		 flags_addr_latch <= 0;
	  end

	  case (op)
		`OP_NOP: begin end

		`OP_LOADA:
		  begin
			 alu_carry <= aluc;
			 F <= flag_or_news;
			 A <= mem_out;
			 C <= mem_out;
			 if (bsel)
			   B <= cube_in;
		  end

		`OP_LOADB:
		  begin
			 alu_sum <= alus;
			 Cond <= inv ? ~flag_or_news : flag_or_news;
			 B <= mem_out;
			 R <= mem_out;
		  end

		`OP_STORE:
		  begin
			 for(idx = 0; idx < 16; idx++) begin
				flags_in[idx] = Cond[idx] ? alu_carry_out[idx] : flags_out[idx];
				latest_news[idx] <= flags_in[idx];
			 end
			 if(flags_addr) begin // we do not write to flag 0
				flags_write <= 1;
				flags_addr_latch <= flags_addr;
			 end
			 mem_in <= alu_sum_out;
			 mem_write <= 1;
		  end

		`OP_STOREI:
		  begin
			 mem_in <= I;
			 mem_write <= 1;
		  end
/*
		`OP_LOADI:
		  begin
			 C <= mem_out;
			 A <= I;
			 alu_sum <= 8'b11110000; // out of A, B, F, select exactly A
		  end
*/

		`OP_LED:
		  begin
			 if(!mode)
			   led_out <= leds;
			 else if(offset == 0)
			   led_out <= mem_out[3:0];
			 else if(offset == 1)
			   led_out <= mem_out[7:4];
			 else if(offset == 2)
			   led_out <= mem_out[11:8];
			 else if(offset == 3)
			   led_out <= mem_out[15:12];
		  end

/*		`OP_RUG:
		  begin
			 if(!rw && ac && !news)
			   begin
				  rug[reg_] <= A;
				  C <= mem_out;
			   end
			 if(!rw && !ac && !news)
			   begin
				  rug[reg_] <= C;
				  A <= mem_out;
			   end
			 if(rw && ac && !news)
			   begin
				  A <= rug[reg_];
				  mem_in <= C;
			   end
			 if(rw && !ac && !news)
			   begin
				  C <= rug[reg_];
				  mem_in <= A;
			   end
			 if(rw && !ac && news)
			   begin
				  R <= mem_out;
				  cube_in <= mem_out;
			   end
			 if(rw && ac && news)
			   begin
				  cube_in <= mem_out;
			   end
		  end
*/
	  endcase
   end
endmodule
