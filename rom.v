// ROM module with single input addr, output port, and clock input.
// Data is clocked out of the ROM on positive clock edges.

module ROM (input clk, input [3:0] addr, output reg [7:0] data);
   always @ (posedge clk) begin
      case(addr)
		4'd0:    data <= 8'b0001_0110; // LDI 6
		4'd1:    data <= 8'b0110_0010; // JP 9
		4'd2:    data <= 8'b0010_0001; // ADD 1
		4'd3:    data <= 8'b1001_0000; // WRITE
		4'd4:    data <= 8'b0110_0010; // JP 2
		4'd5:    data <= 8'b0000_0000;
		4'd6:    data <= 8'b0001_0001; // LDI 1
		4'd7:    data <= 8'b1000_0000; // READ
		4'd8:    data <= 8'b0110_0111; // JP 7
		4'd9:    data <= 8'b1110_0000; // LDQ
		4'd10:   data <= 8'b0001_1100; // LDI 12
		4'd11:   data <= 8'b1010_0000; // CONS
		4'd12:   data <= 8'b1100_0000; // RDQ
		4'd13:   data <= 8'b1101_0000; // CDR
		4'd14:   data <= 8'b1100_0000; // RDQ
		4'd15:   data <= 8'b0000_0000;
		default: data <= 8'bxxxx_xxxx;
	  endcase
   end
endmodule
