#!/usr/bin/perl
use v5.14;
use warnings;
use lib '.';

use asm;

loadi 0, 5;
store 0, 0, 0, 0;
loadi 0, 7;
store 1, 0, 0, 0;

loada 0, 1, 0, alu_select_b;
loadb 1, 0, 1, alu_xor;
store 2, 3, 0, 0;

write_verilog;
