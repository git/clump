#!/usr/bin/perl
use v5.14;
use warnings;
use lib '.';

use asm;

sub flag_to_memory {
	my ($address, $flag) = @_;
	alu3 alu_zero, alu_select_f, 0, 0, $address, $flag, flag_zero;
}

storei 0, 5;
storei 1, 7;

add 0, 1, 0, 4;
flag_to_memory 1, 4;
