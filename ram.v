module RAM #(parameter ADDRESS_BITS = 4)
(input clk, input write, input[ADDRESS_BITS-1:0] addr, input [15:0] in, output reg [15:0] out);

    reg [15:0] memory [0:2**ADDRESS_BITS-1];

   reg [ADDRESS_BITS:0] idx;
   initial begin
	  for(idx = 0; idx < 2**ADDRESS_BITS; idx=idx+1)
		memory[idx] <= 0;
   end

    always @ (negedge clk) begin
        if (write)
		  memory[addr] <= in;
        out <= memory[addr];
    end
endmodule
