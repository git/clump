#!/usr/bin/perl
use v5.14;
use warnings;

my @diffs = (
	[-1,  0],
	[0 ,  1],
	[1 ,  0],
	[0 , -1],
	[-1, -1],
	[-1,  1],
	[1 ,  1],
	[1 , -1],
);

my @cpus;

my @newstable;

my $side = 4; # there are $side * $side CPUs

for my $line (0 .. ($side - 1)) {
	$cpus[$line] = [ ($line * $side) .. (($line + 1) * $side - 1) ]
}

for my $cpu (0 .. ($side * $side - 1)) {
	my $x = $cpu / $side;
	my $y = $cpu % $side;
	for my $direction (0 .. $#diffs) {
		my $nx = ($x + $diffs[$direction][0] + $side) % $side;
		my $ny = ($y + $diffs[$direction][1] + $side) % $side;
		$newstable[$cpu][$direction] = $cpus[$nx][$ny];
	}
}

for my $direction (0 .. $#diffs) {
	print "$direction: news_out = {";
	printf 'news_in[%2d]', $newstable[$side * $side - 1][$direction];
	my @lst = 0 .. ($side * $side - 2);
	for my $cpu (reverse @lst) {
		printf ', news_in[%2d]', $newstable[$cpu][$direction];
	}
	say '};'
}
