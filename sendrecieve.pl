#!/usr/bin/perl
use v5.14;
use warnings;

use Device::SerialPort;
use IPC::Open3;
use Term::ReadLine;
use File::Slurp::Tiny qw/read_file/;

my $term;

if (-t) {
	$term = Term::ReadLine->new('YULE REPL');
	say "YULE REPL\n";
}

my $port = Device::SerialPort->new($ARGV[0] // '/dev/ttyUSB1') or die "$!";
$port->baudrate(300);
#$port->baudrate(4000000);
$port->parity('none');
$port->databits(8);
$port->stopbits(1);
$port->handshake('none');
$port->read_const_time(5000);

$port->write_settings or die "$!";

my $asm = read_file 'cells.clump';

$port->write($asm);
my ($count_in, $string_in) = $port->read(5000);
my @memory = unpack 'v*', $string_in;
for (@memory) {
	printf "%X ", $_
}

__END__

while () {
	my $sexp;
	if (-t) {
		$sexp = $term->readline('* ');
	} else {
		$sexp = <>;
		exit unless defined $sexp;
		chomp $sexp;
		next unless $sexp;
		say "* $sexp";
	}
	exit unless defined $sexp;
	next unless $sexp;
	my $compiler = App::Scheme79asm::Compiler->new;
	my $compiler_out = $compiler->compile_string($sexp);

#	say 'Compiler says: ', dump_sexp($compiler_out);

	my $asm = App::Scheme79asm->new(addr_bits => 13);
	my $asm_output;
	open my $asm_fh, '>', \$asm_output;
	$asm->process($compiler_out);
	$asm->finish;
	$asm->print_binary16($asm_fh);
	close $asm_fh;

#	say "Writing: ", join ' ', uc join ' ', unpack '(H2)*', $asm_output;
	my $bytes_written = $port->write($asm_output);
	my $bytes_to_write = length $asm_output;
	die "Only wrote $bytes_written instead of $bytes_to_write" unless $bytes_written == $bytes_to_write;

	my ($count_in, $string_in) = $port->read(5000);
	my @memory = unpack 'n*', $string_in;
#	say 'Received: ', uc join ' ', unpack '(H2)*', $string_in;
	unshift @memory, 0, 0, (1<<13), (1<<13);
	princ $compiler, \@memory, 6;
	say '';
}
