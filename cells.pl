#!/usr/bin/perl
use v5.14;
use warnings;
use lib '.';

use asm;

sub send_memory_to_news {
	my ($address) = @_;
	alu3 alu_select_a, alu_select_a, $address, $address, $address, flag_zero, flag_zero;
}

sub flag_to_memory {
	my ($address, $flag) = @_;
	alu3 alu_zero, alu_select_f, 0, 0, $address, $flag, flag_zero;
}

my $flag_F = 1;

sub read_write_two_ways {
	my ($news) = @_;
	my $memory = $news + 1;
	send_memory_to_news 0;
	alu3 alu_zero, alu_select_f, 0, 0, $memory, flag_news($news), flag_zero;
	send_memory_to_news 0;
	alu3 aluc_addAF, alus_addAF, $memory, 0, $memory, flag_news($news + 1), $flag_F;
	flag_to_memory $memory + 1, $flag_F;
}

sub conway_step {
	#say "CONWAY STEP";
	# write memory 0 to news
	# read from news direction 0 into memory 1
	# write memory 0 to news
	# read from news direction 1, add to memory 1, write into memory 1 and flag F
	# write flag F to memory 2
	read_write_two_ways(0);

	# write memory 0 to news
	# read from news direction 2 into memory 3
	# write memory 0 to news
	# read from news direction 3, add to memory 3, write into memory 3 and flag F
	# write flag F to memory 4
	read_write_two_ways(2);

	# write memory 0 to news
	# read from news direction 4 into memory 5
	# write memory 0 to news
	# read from news direction 5, add to memory 5, write into memory 5 and flag F
	# write flag F to memory 6
	read_write_two_ways(4);

	# write memory 0 to news
	# read from news direction 6 into memory 7
	# write memory 0 to news
	# read from news direction 7, add to memory 7, write into memory 7 and flag F
	# write flag F to memory 8
	read_write_two_ways(6);

	# read from memory 1, add to memory 3, write into memory 1 and flag F
	add 1, 3, 1, $flag_F;
	# read from memory 2, add to memory 4 and flag F, write into memory 2 and flag F
	addC 2, 4, 2, $flag_F;
	# write flag F to memory 3
	flag_to_memory 3, $flag_F;

	# read from memory 5, add to memory 7, write into memory 5 and flag F
	add 5, 7, 5, $flag_F;
	# read from memory 6, add to memory 8 and flag F, write into memory 6 and flag F
	addC 6, 8, 6, $flag_F;
	# write flag F to memory 7
	flag_to_memory 7, $flag_F;

	# read from memory 1, add to memory 5, write into memory 1 and flag F
	add 1, 5, 1, $flag_F;
	# read from memory 2, add to memory 6 and flag F, write into memory 2 and flag F
	addC 2, 6, 2, $flag_F;
	# read from memory 3, or with memory 7 and flag F, write into flag F
	alu3 alu_or, alu_select_a, 3, 7, 3, $flag_F, $flag_F;

	# should a new cell be born here? (do we have 3 neighbours?)
	sub alu_birth { alu_of_function {; $a && $b && !$_ } }

	# should a living cell survive here? (do we have 2 or 3 neighbours?)
	sub alu_survival { alu_of_function {; $a && !$_  } }

	# compute the state of this cell at the next tick
	sub alu_step { alu_of_function {; $_ & ($a | $b) } }

	# read from memory 1, and with memory 2 and not F, write into memory 1 (= birth-p)
	alu3 alu_zero, alu_birth, 1, 2, 1, $flag_F, flag_zero;
	# read from memory 2, and with flag not F, write into flag F (= survive-p)
	alu3 alu_survival, alu_select_a, 2, 0, 2, $flag_F, $flag_F;
	# read from memory 0, memory 1, and flag F, write F and (mem0 or mem1) into memory 0
	alu3 alu_zero, alu_step, 0, 1, 0, $flag_F, $flag_F; # also zeroes out flag F

	read_ 0;
#	say 'Press enter to continue:';
#	my $nope = <>;
#	say 'Pressed, continuing';
	read_ 0;
#	read_ 1;
#	read_ 2;
#	read_ 3;
#	read_ 4;
#	read_ 5;
#	read_ 6;

	#sleep 10;
}

$| = 1;

# start with a blinker
read_ 0;
loadi 0, 0x2022;
#sleep 100;

store 0, flag_zero, 0, 0;
read_ 0;
read_ 0;
#sleep 10;

conway_step;
conway_step;
conway_step;
conway_step;
conway_step;
conway_step;
