`include "pll.v"
`include "master_rom.v"
`include "i2c.v"
`include "uart.v"

`ifdef SIM
 `define UART_DIVIDE 1
 `define I2C_DIVIDE 4
`else
 `define UART_DIVIDE 1024
 `define I2C_DIVIDE 256
`endif

module master(input CLKin, output [4:0] led, output uart_tx, input uart_rx, output reg busy_out = 0, input busy_in, output scl, output sda);
   wire clk;

   assign clk = CLKin;

   // ROM

   reg [7:0] program_counter = 0;
   // go here at end of program
   reg [7:0] main = 1;
   wire [31:0] rom_output;

   master_rom master_rom (.clk(clk), .addr(program_counter), .data(rom_output));

   wire [2:0]  rom_op = rom_output[26:24];
   wire [2:0]  rom_led = rom_output[14:12];
   wire [3:0]  rom_chip_select = rom_output[31:28];

   // if the 4th board won't execute this instruction,
   // then we won't receive propagation or news
   wire 	   dont_wait = !rom_chip_select[3];
   reg [25:0]  dont_send = 23'b11111111111111111111111;

   always @(posedge clk) begin
	  if(busy_in)
		dont_send <= 21'b111111111111111111111;
	  else if(dont_send)
		dont_send <= dont_send - 1;
   end

   // state

`define STATE_SEND 0
`define STATE_WAIT_PROPAGATE 1
`define STATE_WAIT_NEWS 2
`define STATE_PROPAGATE_NEWS 3
`define STATE_SET_LEDS 4
`define STATE_WASTE_TIME 5

   reg [5:0] state = `STATE_SEND;
   reg [5:0] uart_ptr = 0;

   reg [30:0]  waste_counter = 0;
   reg [7:0]   saved_news [3:0];
   reg [7:0]   sent_byte [3:0];

   reg [7:0]   bytes_sent = 0;

   // i2c
   reg [15:0]  leds [3:0];

   initial begin
	  leds[0] <= 16'hF00F;
	  leds[1] <= 16'h0000;
	  leds[2] <= 16'h0000;
	  leds[3] <= 16'hFFFF;
   end

   /* even rows are green, odd rows are red:
    * mb_leds[2 * k] is the kth row of green leds
	* mb_leds[2 * k + 1] is the kth row of red leds
	*/
   wire [7:0] mb_leds [15:0];

   // all red leds are off
   assign mb_leds[1] = program_counter;
   assign mb_leds[3] = 0;
   assign mb_leds[5] = state;
   assign mb_leds[7] = uart_ptr;
   assign mb_leds[9] = 0;
   assign mb_leds[11] = 0;
   assign mb_leds[13] = 0;
   assign mb_leds[15] = 0;

   // green leds, first half
   assign mb_leds[0] = {leds[1][3:0],   leds[0][3:0]};
   assign mb_leds[2] = {leds[1][7:4],   leds[0][7:4]};
   assign mb_leds[4] = {leds[1][11:8],  leds[0][11:8]};
   assign mb_leds[6] = {leds[1][15:12], leds[0][15:12]};

   // green leds, second half
   assign mb_leds[8]  = {leds[3][3:0],   leds[2][3:0]};
   assign mb_leds[10] = {leds[3][7:4],   leds[2][7:4]};
   assign mb_leds[12] = {leds[3][11:8],  leds[2][11:8]};
   assign mb_leds[14] = {leds[3][15:12], leds[2][15:12]};

   wire [7:0]  mb_in;
   wire [5:0]  mb_addr;
   assign mb_in = mb_leds[mb_addr];

   reg [7:0]   i2c_tx_byte;
   reg [5:0]   more_bytes = 0;
   reg 		   i2c_transmit = 0;
   wire 	   i2c_is_transmitting;

   i2c_write #(.CLOCK_DIVIDE(`I2C_DIVIDE)) i2c (.clk(clk), .scl(scl), .sda(sda), .tx_byte(i2c_tx_byte), .transmit(i2c_transmit), .is_transmitting(i2c_is_transmitting), .more_bytes(more_bytes), .mb_in(mb_in), .mb_addr(mb_addr));

   reg [3:0]   i2c_init_step = 0;

   always @ (posedge clk) begin
	  if(i2c_is_transmitting || i2c_transmit)
		i2c_transmit <= 0;
	  else begin
		 if(i2c_init_step == 0) begin
			i2c_tx_byte <= 8'h21; // turn on oscillator
			i2c_transmit <= 1;
			i2c_init_step <= 1;
		 end else if(i2c_init_step == 1) begin
			i2c_tx_byte <= 8'h81; // display on, blink off
			i2c_transmit <= 1;
			i2c_init_step <= 2;
		 end else if(i2c_init_step == 2) begin
			i2c_tx_byte <= 8'hEF; // max brightness
			i2c_transmit <= 1;
			i2c_init_step <= 3;
		 end else if(i2c_init_step == 3) begin
			i2c_tx_byte <= 0;
			more_bytes <= 16;
			i2c_transmit <= 1;
			i2c_init_step <= 4;
		 end else begin
			i2c_transmit <= 1;
		 end
	  end
   end


   wire 	   received;
   wire [7:0]  rx_byte;
   reg 		   transmit = 0;
   reg  [7:0]  tx_byte = 0;
   wire 	   is_receiving;
   wire 	   is_transmitting;

   uart #(.CLOCK_DIVIDE(`UART_DIVIDE)) uart (.clk(clk), .rx(uart_rx), .tx(uart_tx), .received(received), .transmit(transmit), .tx_byte(tx_byte), .rx_byte(rx_byte), .is_receiving(is_receiving), .is_transmitting(is_transmitting));

   assign led[4] = state != `STATE_WASTE_TIME;
//   assign led[2:0] = rom_op == 6 ? rom_led : 0;
//   assign led[3] = refresh_leds || i2c_is_transmitting;
//   assign led[0] = (leds[0] == 16'h000f);
//   assign led[1] = (leds[0] == 16'h00f0);
//   assign led[2] = (leds[0] == 16'h0111);
//   assign led[3] = (leds[0] == 16'hf00f);


   //assign led[3:0] = saved_news[0][3:0];
   //assign led[3:0] = program_counter[3:0];
   assign led[0] = dont_send;
   assign led[1] = dont_wait;
   assign led[2] = is_transmitting;

   always @(posedge clk) begin
	  case(state)
		`STATE_SEND: begin
		   if(transmit) begin
			  transmit <= 0;
		   end else if(uart_ptr == 4) begin
			  uart_ptr <= 0;
			  if(dont_wait)
				state <= `STATE_WASTE_TIME;
			  else if(rom_op == 6) // `OP_ROUTE
				state <= `STATE_WAIT_NEWS;
			  else
				state <= `STATE_WAIT_PROPAGATE;
		   end else if(!is_transmitting && !dont_send) begin
			  tx_byte <= rom_output[uart_ptr * 8 +: 8];
			  sent_byte[uart_ptr] <= rom_output[uart_ptr * 8 +: 8];
			  transmit <= 1;
			  bytes_sent <= bytes_sent + 1;
			  uart_ptr <= uart_ptr + 1;
		   end
		end

		`STATE_WAIT_PROPAGATE: begin
		   if(received) begin
			  state <= `STATE_WASTE_TIME;
		   end
		end

		`STATE_WASTE_TIME: begin
		   if(waste_counter == 50000) begin
			  waste_counter <= 0;
			  if(program_counter == 255)
				program_counter <= main;
			  else
				program_counter <= program_counter + 1;
			  state <= `STATE_SEND;
		   end else
			 waste_counter <= waste_counter + 1;
		end

		`STATE_WAIT_NEWS: begin
		   /** On a route instruction, we:
			   - receive the instruction back
			   - receive the news
			   - propagate the news
			   - go to `STATE_WASTE_TIME
			*/
		   if(uart_ptr == 8) begin
			  state <= `STATE_PROPAGATE_NEWS;
			  uart_ptr <= 0;
		   end else if(received) begin
			  if(uart_ptr[2]) /* uart_ptr >= 4 */
				saved_news[uart_ptr[1:0]] <= rx_byte;
			  uart_ptr <= uart_ptr + 1;
		   end
		end // case: `STATE_WAIT_NEWS

		`STATE_PROPAGATE_NEWS: begin
		   if(transmit) begin
			  transmit <= 0;
		   end else if(uart_ptr == 4) begin
			  if(rom_led) begin
				 leds[rom_led - 1] <= (saved_news[1] << 8) + saved_news[0];
			  end
			  state <= `STATE_WASTE_TIME;
			  uart_ptr <= 0;
		   end else if(!is_transmitting && !dont_send) begin
			  tx_byte <= saved_news[uart_ptr];
			  sent_byte[uart_ptr] <= saved_news[uart_ptr];
			  transmit <= 1;
			  bytes_sent <= bytes_sent + 1;
			  uart_ptr <= uart_ptr + 1;
		   end
		end // case: `STATE_PROPAGATE_NEWS
	  endcase
   end

endmodule
