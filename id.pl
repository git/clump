#!/usr/bin/perl
use v5.14;
use warnings;
use lib '.';

use asm;

chip_select 1, sub {
	storei 0, 0x3;
};

chip_select 2, sub {
	storei 0, 0x2;
};

chip_select 4, sub {
	storei 0, 0x4;
};

chip_select 8, sub {
	storei 0, 0x8;
};

ledm 0;
