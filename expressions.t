'(1 2 3)
5
((lambda id (x) x) 5)
((lambda id (x) x) '(1 2 3))
(car '(2 3))
(car (cdr '(4 5 6)))
((lambda snd (x y) y) 'first 'second)

(cons '(1 2) '(7))
(car (cons '(1 2) '(7)))
(cdr (cons '(1 2) '(7)))

((lambda rev (xs) ((lambda reverse-acc (xs acc) (if xs (reverse-acc (cdr xs) (cons (car xs) acc)) acc)) xs '())) '(4 5 6 7))

(atom nil)
(atom t)
(atom 'symbol)
(atom 5)

(reverse-list 9 8 7 6 5 4)

(atom '())
(atom '(1 2 3))
(atom '((1 2) (3 4)))
(atom (cons 1 '(2)))

(progn 4 5 6 7 8)
