#!/usr/bin/perl
use v5.14;
use warnings;

use Device::SerialPort;

my $program;
read STDIN, $program, 1000000;

my $port = Device::SerialPort->new($ARGV[0] // '/dev/ttyUSB1') or die "$!";
$port->baudrate(300);
$port->parity('none');
$port->databits(8);
$port->stopbits(2);
$port->handshake('none');

$port->write_settings or die "$!";

my $bytes_written = $port->write($program);
my $bytes_to_write = length $program;
die "Only wrote $bytes_written instead of $bytes_to_write" unless $bytes_written == $bytes_to_write;
