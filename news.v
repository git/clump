module news(input clk, input [15:0] news_in, input [2:0] direction, output [0:15] news_out);
   always @* begin
		 case (direction)
0: news_out = {news_in[11], news_in[10], news_in[ 9], news_in[ 8], news_in[ 7], news_in[ 6], news_in[ 5], news_in[ 4], news_in[ 3], news_in[ 2], news_in[ 1], news_in[ 0], news_in[15], news_in[14], news_in[13], news_in[12]};
1: news_out = {news_in[12], news_in[15], news_in[14], news_in[13], news_in[ 8], news_in[11], news_in[10], news_in[ 9], news_in[ 4], news_in[ 7], news_in[ 6], news_in[ 5], news_in[ 0], news_in[ 3], news_in[ 2], news_in[ 1]};
2: news_out = {news_in[ 3], news_in[ 2], news_in[ 1], news_in[ 0], news_in[15], news_in[14], news_in[13], news_in[12], news_in[11], news_in[10], news_in[ 9], news_in[ 8], news_in[ 7], news_in[ 6], news_in[ 5], news_in[ 4]};
3: news_out = {news_in[14], news_in[13], news_in[12], news_in[15], news_in[10], news_in[ 9], news_in[ 8], news_in[11], news_in[ 6], news_in[ 5], news_in[ 4], news_in[ 7], news_in[ 2], news_in[ 1], news_in[ 0], news_in[ 3]};
4: news_out = {news_in[10], news_in[ 9], news_in[ 8], news_in[11], news_in[ 6], news_in[ 5], news_in[ 4], news_in[ 7], news_in[ 2], news_in[ 1], news_in[ 0], news_in[ 3], news_in[14], news_in[13], news_in[12], news_in[15]};
5: news_out = {news_in[ 8], news_in[11], news_in[10], news_in[ 9], news_in[ 4], news_in[ 7], news_in[ 6], news_in[ 5], news_in[ 0], news_in[ 3], news_in[ 2], news_in[ 1], news_in[12], news_in[15], news_in[14], news_in[13]};
6: news_out = {news_in[ 0], news_in[ 3], news_in[ 2], news_in[ 1], news_in[12], news_in[15], news_in[14], news_in[13], news_in[ 8], news_in[11], news_in[10], news_in[ 9], news_in[ 4], news_in[ 7], news_in[ 6], news_in[ 5]};
7: news_out = {news_in[ 2], news_in[ 1], news_in[ 0], news_in[ 3], news_in[14], news_in[13], news_in[12], news_in[15], news_in[10], news_in[ 9], news_in[ 8], news_in[11], news_in[ 6], news_in[ 5], news_in[ 4], news_in[ 7]};
		 endcase

   end
endmodule
