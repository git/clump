`define STATE_START        3'b000
`define STATE_WRITE1_WAIT  3'b001
`define STATE_WRITE1       3'b010
`define STATE_WRITE2_WAIT  3'b011
`define STATE_WRITE2       3'b100
`define STATE_INCREMENT    3'b101
`define STATE_FINISHED     3'b111

module WRITER (input clk, input clk_enable, output [7:0] tx_byte, output tx_signal, input tx_busy, output finished, output [12:0] ram_addr, input [15:0] ram_do, input [12:0] freeptr);
   reg [2:0] state = `STATE_START;
   reg [12:0] current_index;

   assign ram_addr = current_index;
   assign finished = state == `STATE_FINISHED;

   always @* begin
	  case(state)
		`STATE_WRITE1: begin
		   tx_signal <= 1;
		   tx_byte   <= ram_do[15:8];
		end

		`STATE_WRITE2: begin
		   tx_signal <= 1;
		   tx_byte   <= ram_do[7:0];
		end

		default: begin
		   tx_signal <= 0;
		   tx_byte   <= 12'dx;
		end
	  endcase
   end

   always @ (posedge clk) begin
	 if (clk_enable) begin
		case(state)
		  `STATE_START: begin
			 current_index <= 4;
			 state <= `STATE_WRITE1_WAIT;
		  end

		  `STATE_WRITE1_WAIT: begin
			 if(!tx_busy) state <= `STATE_WRITE1;
		  end

		  `STATE_WRITE1: state <= `STATE_WRITE2_WAIT;

		  `STATE_WRITE2_WAIT: begin
			 if(!tx_busy) state <= `STATE_WRITE2;
		  end

		  `STATE_WRITE2: state <= `STATE_INCREMENT;

		  `STATE_INCREMENT: begin
			 current_index <= current_index + 1;
			 if(current_index >= freeptr) begin
				state <= `STATE_FINISHED;
			 end else begin
				state <= `STATE_WRITE1_WAIT;
			 end
		  end

		  `STATE_FINISHED: state <= `STATE_START;

		  default: state <= `STATE_START;
		endcase // case (state)
	 end // if (clk_enable)
   end
endmodule
