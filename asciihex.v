// convert ASCII 0-9A-Za-z to/from numbers

module ascii_to_hex(input [7:0] ascii, output [3:0] hex);
   wire [7:0] hex8;
   assign hex8 =
			   ascii > 8'd47 & ascii < 8'd58  ? ascii - 8'd48 :
			   ascii > 8'd64 & ascii < 8'd71  ? ascii - 8'd55 :
			   ascii > 8'd96 & ascii < 8'd103 ? ascii - 8'd87 : 8'bxxxx;
   assign hex = hex8[3:0];

/*   assign hex =
			   ascii == 8'd48 ? 4'd0  :
               ascii == 8'd49 ? 4'd1  :
               ascii == 8'd50 ? 4'd2  :
               ascii == 8'd51 ? 4'd3  :
               ascii == 8'd52 ? 4'd4  :
               ascii == 8'd53 ? 4'd5  :
               ascii == 8'd54 ? 4'd6  :
               ascii == 8'd55 ? 4'd7  :
               ascii == 8'd56 ? 4'd8  :
               ascii == 8'd57 ? 4'd9  :
               ascii == 8'd65 ? 4'd10 :
               ascii == 8'd66 ? 4'd11 :
               ascii == 8'd67 ? 4'd12 :
               ascii == 8'd68 ? 4'd13 :
               ascii == 8'd69 ? 4'd14 : 4'd15;*/
endmodule // ascii_to_hex

module hex_to_ascii(input [3:0] hex, output [7:0] ascii);
//   assign ascii = hex < 4'd10 ? 8'd48 + hex : 8'd55 + hex;
   assign ascii =
				 hex == 4'd0  ? 8'd48 :
				 hex == 4'd1  ? 8'd49 :
				 hex == 4'd2  ? 8'd50 :
				 hex == 4'd3  ? 8'd51 :
				 hex == 4'd4  ? 8'd52 :
				 hex == 4'd5  ? 8'd53 :
				 hex == 4'd6  ? 8'd54 :
				 hex == 4'd7  ? 8'd55 :
				 hex == 4'd8  ? 8'd56 :
				 hex == 4'd9  ? 8'd57 :
				 hex == 4'd10 ? 8'd65 :
				 hex == 4'd11 ? 8'd66 :
				 hex == 4'd12 ? 8'd67 :
				 hex == 4'd13 ? 8'd68 :
				 hex == 4'd14 ? 8'd69 :
				 hex == 4'd15 ? 8'd70 : 8'bx;
endmodule // hex_to_ascii
