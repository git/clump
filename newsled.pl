#!/usr/bin/perl
use v5.14;
use warnings;
use lib '.';

use asm;

storei 0, 4;
storei 1, 2;
ledm   1, 0;
nop;
nop;
route  0, 1;
ledm   1, 0;
nop;
nop;
storei 1, 3;
ledm   1, 0;
nop;
nop;
route  0, 1;
ledm   1, 0;
nop;
