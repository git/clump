// Take variable duration pulse which may be high at any time and generate multiple cycle
// high pulses alligned with the positive edge of the clock pulse.

module MULTIPLE_TRIGGER  #(parameter BITS = 4) (input clk, input trigger_in, output[BITS-1:0] trigger_out);

   reg [BITS-1:0] trigger = 0;

   reg last_trigger_in = 0;
   
   always @ (posedge clk) begin

      trigger <= { trigger[BITS-2:0], (!last_trigger_in & trigger_in) };

      last_trigger_in <= trigger_in;
      
   end

   assign trigger_out = trigger;
   
endmodule
